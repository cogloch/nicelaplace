/////////////////////////////////////////
//
//        Comp Aero 2019
//
//      George Cucu, gc16397
//
/////////////////////////////////////////


// Link libs 
#pragma comment(lib, "glfw3.lib")
#pragma comment(lib, "imgui.lib")
#pragma comment(lib, "opengl32.lib")


#include <imgui/imgui.h>
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include <stdio.h>

#include <glad/glad.h>  
#include <GLFW/glfw3.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <algorithm>
#include <map>
#include <fstream>

/////////////////////
// TODO
/////////////////////
// First cell AR
// TE/LE clustering param (weighted angle increments within octant)
// Cmdline args!!!!!
// Cleanup
// Report
// Submit


// Use single (32bit) precision throughout:
//       - to be consistent with ImGui (the GUI toolkit) and the rendering backend 
//       - to be friendly to GPUs 
//               - of course I'm not using the GPU for computing the mesh, 
//                 but assume the mesh were to be used for some actually 
//                 useful computation later, on the GPU 

// Note on input limits:
//   In certain places, the chosen limits are arbitrary (e.g. maximum nfar). 
//   A more "rigurous" approach would be setting limits based on what can be represented:
//              - In memory: say the end user for some reason wants to load the whole grid on the stack
//                           say using the default stack size on MSVC (1MiB)
//                           say the stack would be used only for this (of course it wouldn't) 
//                           -> limit no. of gridpoints to 262,144   
//              - Numerically: based on FLT_EPSILON(std::numeric_limits<float>::epsilon()),
//                             and FLT_MAX(std::numeric_limits<float>::max())
//
//   But imposing actually non-arbitrary limits like this would make them interdependent,
//   and would also putting a limit on/changing how rendering resources are handled (for the preview)
//               -> would take too much time 
struct GridParams
{
    // Thickness/chord ratio in [0,1)
    float tc = 0.2f;
    const float tc_MIN = 0.f, 
                tc_MAX = (1.f - (float)1.0e-4);

    // Grid extent (square)
    int nfar = 2;
    const int nfar_MIN = 2,
              nfar_MAX = 1000;         // Arbitrary

    // Lines of constant angular coord
    int imax = 9;
    const int imax_MIN = 9,
              imax_MAX = 999;         // Arbitrary

    // Lines of constant radial coord
    int jmax = 3;
    const int jmax_MIN = 3,
              jmax_MAX = 1000;        // Arbitrary

    enum FirstCellOption {
        HEIGHT, AR, NONE
    };
    int firstCellOption;
    float firstCellOptValue = 1.f;

    float minAr = 0.f, maxAr = 100.f; // Will be computed along
};

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

GLFWwindow* Init()
{
    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        throw "Init:: glfw init failed";

    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(800, 600, "Compaero 2019 Meshgen", NULL, NULL);
    if (!window)
        throw "Init:: window creation failed";
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    if (!gladLoadGL())
        throw "Init:: OGL loader failed";

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    return window;
}

void PlotPoint(ImDrawList* drawList, const ImVec2& canvasTopLeft, const ImVec2& pointTopLeft, ImVec4 col = ImVec4(1.f, 1.f, 1.f, 1.f), float pointSz = 2.5f)
{
    ImU32 col32 = ImColor(col);
    const ImVec2 topLeft(canvasTopLeft.x + pointTopLeft.x, canvasTopLeft.y + pointTopLeft.y);
    const ImVec2 botRight(topLeft.x + pointSz, topLeft.y + pointSz);
    drawList->AddRectFilled(topLeft, botRight, col32);
}

static void HelpMarker(const char* desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

// Double precision for the actual data
// Single precision for rendering jobs (cast to ImVec2, and shove that to ImGui) 
struct Vec2
{
    double x, y;

    Vec2() : x(0.0), y(0.0) {}
    Vec2(double x_, double y_) : x(x_), y(y_) {}

    // Demote to single precision for rendering 
    operator ImVec2() const { return ImVec2(static_cast<float>(x), static_cast<float>(y)); }
};

struct Drawable
{

    bool hidden = false;

    enum class Topology
    {
        POINTS, LINES
    } topology;

    //std::vector<Vec2> vertsLocal;
    std::vector<ImVec2> vertsLocal;

    // Map coordinates in local space (chord = 1.0) onto screen rect
    std::vector<ImVec2> vertsScreen;

    void UpdateScreenCoords(ImVec2 canvasScreenPos, ImVec2 canvasScreenSize, int spaceSize /* == "Nfar" */)
    {
        // Screen space:
        // canvasScreenPos [0] ------------------------------                   
        // |                                                |
        // -------------------------------- canvasScreenPos + canvasScreenSize

        // Canvas space:
        // - canvasScreenSize / 2 -----------------------------------                   
        // |                          0                             |
        // --------------------------------------------- + canvasScreenSize / 2

        if (vertsScreen.size() != vertsLocal.size())
            vertsScreen.resize(vertsLocal.size());

        // Scale by half canvasScreenSize / Nfar      (space is 2Nfar x 2Nfar)
        const float scaleFactor = static_cast<float>(canvasScreenSize.x) / 
                                   static_cast<float>(spaceSize) / 2.f;
        for (size_t i = 0; i < vertsLocal.size(); ++i)
        {
            const auto& local = vertsLocal[i];
            auto& screen = vertsScreen[i];

            // 1. Scale in local space 
            screen.x = local.x * scaleFactor;
            screen.y = local.y * scaleFactor;

            // 2. Translate to canvas "origin" (centre)
            screen.x += canvasScreenPos.x + canvasScreenSize.x / 2.0;
            screen.y += canvasScreenPos.y + canvasScreenSize.y / 2.0;
        }
    }

    Drawable(Topology topology_ = Topology::POINTS) : topology(topology_) {}

    void Draw(ImDrawList& drawList) const
    {
        if (hidden)
            return;

        switch (topology)
        {
        case Topology::POINTS:
            for (auto& vert : vertsScreen)
                drawList.AddCircleFilled(vert, 1.f, IM_COL32(255, 255, 255, 255));
            break;
        case Topology::LINES:
            drawList.AddPolyline(vertsScreen.data(), vertsScreen.size(), IM_COL32(255, 0, 0, 255), false, .1f);
            break;
        }
    }
};

struct Canvas
{
    ImVec2 pos;
    ImVec2 size;
};

Canvas CreateAvailableCanvas(ImDrawList& drawList)
{
    ImVec2 pos = ImGui::GetCursorScreenPos();           
    ImVec2 size = ImGui::GetContentRegionAvail();       

    size.x = std::clamp(size.x, 50.f, size.x);
    size.y = std::clamp(size.y, 50.f, size.y);
    
    // Force square 
#undef min // windows.h is being silly 
    const float availableSquareSz = std::min(size.x, size.y);
    size.x = availableSquareSz;
    size.y = availableSquareSz;

    drawList.AddRectFilledMultiColor(pos, ImVec2(pos.x + size.x, pos.y + size.y), IM_COL32(50, 50, 50, 255), IM_COL32(50, 50, 60, 255), IM_COL32(60, 60, 70, 255), IM_COL32(50, 50, 60, 255));
    drawList.AddRect(pos, ImVec2(pos.x + size.x, pos.y + size.y), IM_COL32(255, 255, 255, 255));

    return { pos, size };
}

// @tc: thickness/chord ratio in [0..1)
Drawable ComputeFoil(const double tc)
{
    //const double epsilon = std::numeric_limits<double>::epsilon();
    const float epsilon = (float)1.0e-4;
    if (tc < 0.f || tc > (1.f - epsilon))
        throw "Thickness/chord ratio must be within [0,1).";

    const int numSamples = 30;
    const float chord = 1.f;
    const float minX = -chord / 2.f;
    const float dx = chord / numSamples;

    const float semiMaj = chord / 2.f;
    const float semiMin = (chord * tc) / 2.f;

    Drawable foil;

    for (int i = 0; i <= numSamples; ++i)
    {
        float x = minX + dx * i;
        float y = semiMin / semiMaj * sqrtf(semiMaj * semiMaj - x * x);
        foil.vertsLocal.push_back(Vec2(x, y));
        foil.vertsLocal.push_back(Vec2(x, -y));
    }

    return foil;
}

// @tc: thickness/chord ratio in [0..1)
Drawable ComputeFoci(const double tc)
{
    //const double epsilon = std::numeric_limits<double>::epsilon();
    const float epsilon = (float)1.0e-4;
    if (tc < 0.f || tc > (1.f - epsilon))
        throw "Thickness/chord ratio must be within [0,1).";

    const float chord = 1.f;
    const float semiMaj = chord / 2.f;
    const float semiMin = (chord * tc) / 2.f;

    float focus = sqrtf(semiMaj * semiMaj - semiMin * semiMin);
    Drawable foci;
    foci.vertsLocal.push_back({ -focus, 0.f });
    foci.vertsLocal.push_back({  focus, 0.f });
    return foci;
}

bool toExport = false;

void AddUIControls(GridParams& input)
{
    ImGui::Text("Tip: CTRL+Click on sliders for text input.");

    // imax
    ImGui::SliderInt("Circumf. points (imax)", &input.imax, input.imax_MIN, input.imax_MAX);
    input.imax = std::clamp(input.imax, input.imax_MIN, input.imax_MAX);
    input.imax = static_cast<int>((static_cast<float>(input.imax) / 8.f)) * 8 + 1;
    ImGui::SameLine(); HelpMarker("= 8n+1");

    // jmax
    ImGui::SliderInt("Radial points (jmax)", &input.jmax, input.jmax_MIN, input.jmax_MAX);
    input.jmax = std::clamp(input.jmax, input.jmax_MIN, input.jmax_MAX);

    // t/c 
    ImGui::SliderFloat("t/c", &input.tc, input.tc_MIN, input.tc_MAX, "%.4f");
    input.tc = std::clamp(input.tc, input.tc_MIN, input.tc_MAX);
    ImGui::SameLine(); HelpMarker("In [0,1)");

    // Nfar
    ImGui::SliderInt("Space size", &input.nfar, input.nfar_MIN, input.nfar_MAX, "%d chords");
    ImGui::SameLine();
    HelpMarker("= Nfar \n Interpreted brief as \"integer multiple of chord\"");

    // First cell
    ImGui::RadioButton("Set first cell height", &input.firstCellOption, input.HEIGHT); ImGui::SameLine();
    ImGui::RadioButton("Set first cell AR", &input.firstCellOption, input.AR); ImGui::SameLine();
    ImGui::RadioButton("No first cell restriction", &input.firstCellOption, input.NONE);

    switch (input.firstCellOption)
    {
    case input.HEIGHT:
        // 95% of space (to not touch outer boundary with main O fit) 
        // minus half chord 
        //ImGui::SliderFloat("First cell height", &input.firstCellOptValue, 0.1f, (0.95f * input.nfar - 0.5f));
        ImGui::SliderFloat("First cell height", &input.firstCellOptValue, 0.1f, input.nfar * 0.5f, "%.3f chords");
        // TODO: Clamp limits
        break;
    case input.AR:
        ImGui::SliderFloat("First cell AR", &input.firstCellOptValue, input.minAr, input.maxAr);
        // TODO: Clamp limits
        break;
    }

    if (ImGui::Button("Export to Tecplot"))
    {
        toExport = true;
    }
}

// Get radial coordinate corresponding to the first ellipse that intersects the (x, @y) line 
float IntersectRadialY(const float focus, const float y)
{
    return asinhf(y / focus);
}

float IntersectRadialX(const float focus, const float x)
{
    return acoshf(x / focus);
}

ImVec2 operator - (const ImVec2& lhs, const ImVec2& rhs)
{
    return ImVec2(lhs.x - rhs.x, lhs.y - rhs.y);
}

float Length(const ImVec2& vec)
{
    return sqrtf(vec.x * vec.x + vec.y * vec.y);
}

float innerFoc, innerMu;

std::vector<std::vector<ImVec2>> ComputeGrid(const GridParams& input, Drawable& drawableGrid)
{
    const float chord = 1.f;
    const float semiMaj = chord / 2.f;                       // Semimajor
    const float semiMin = (chord * input.tc) / 2.f;          // Semiminor
    const float focus = sqrtf(semiMaj * semiMaj - semiMin * semiMin);

    // The rectangular outer boundary is not part of the mapping 
    std::map<float, ImVec2> cornerBC = {
        {0.f * M_PI_4, ImVec2(input.nfar, 0.f)},
        {1.f * M_PI_4, ImVec2(input.nfar, input.nfar)},
        {2.f * M_PI_4, ImVec2(0.f, input.nfar)},
        {3.f * M_PI_4, ImVec2(-input.nfar, input.nfar)},
        {4.f * M_PI_4, ImVec2(-input.nfar, 0.f)},
        {5.f * M_PI_4, ImVec2(-input.nfar, -input.nfar)},
        {6.f * M_PI_4, ImVec2(0.f, -input.nfar)},
        {7.f * M_PI_4, ImVec2(input.nfar, -input.nfar)},
    };

    // Angular isolines 
    std::vector<float> nus;
    int sectorsPerOctant = (input.imax - 1) / 8;
    double dangle = M_PI_4 / sectorsPerOctant;
    for (int i = 0; i < input.imax; ++i)
        nus.push_back(dangle * i);

    // Radial isolines
    const float oMapLimit = 0.95;
    float surfMu = IntersectRadialY(focus, semiMin);
    float maxmu = std::min(IntersectRadialY(focus, oMapLimit * input.nfar),
                           IntersectRadialX(focus, oMapLimit * input.nfar));

    static bool useFirstCell = true;

    std::vector<float> mus;
    
    if (useFirstCell)
    {
        // Surface
        mus.push_back(surfMu);
        // First cell 
        float firstMu = surfMu + 0.1 * surfMu; // Default - used if height/AR options fail
        switch (input.firstCellOption)
        {
        case GridParams::FirstCellOption::HEIGHT:
        {
            firstMu = IntersectRadialY(focus, semiMin + input.firstCellOptValue);
            float tol = 0.01f;

            float h = input.firstCellOptValue;
            float foc = 0.f;
            float mu = 0.f;
            float x = 0.f, y = 0.f;
            bool found = false;
            for (; foc < input.nfar; foc += 0.01f)
            {
                for (mu = 0.f; mu < input.nfar; mu += 0.01f)
                {
                    x = foc * coshf(mu);
                    y = foc * sinh(mu);

                    if (fabs(x - (semiMaj + h)) < tol && fabs(y - (semiMin + h)) < tol)
                    {
                        found = true;
                        break;
                    }
                }

                if (found)
                    break;
            }

            if (found)
            {
                innerFoc = foc;
                innerMu = mu;
                firstMu = innerMu;
                maxmu = std::min(IntersectRadialY(innerFoc, oMapLimit * input.nfar),
                                 IntersectRadialX(innerFoc, oMapLimit * input.nfar));
                //__debugbreak();
            }
            else
            {
                innerFoc = -1.f;
                innerMu = -1.f;
            }
            break;
        }
        case GridParams::FirstCellOption::AR:
        {
            ImVec2 a;
            a.x = focus * coshf(surfMu) * cos(nus[1]);
            a.y = focus * sinhf(surfMu) * sin(nus[1]);
            ImVec2 b;
            b.x = focus * coshf(surfMu) * cos(nus[0]);
            b.y = focus * sinhf(surfMu) * sin(nus[0]);
            ImVec2 c;
            c.x = focus * coshf(surfMu) * cos(nus[nus.size()-2]);   //nus.back() is i==imax overlapping i==0
            c.y = focus * sinhf(surfMu) * sin(nus[nus.size()-2]);
            float leftLen = Length(b - a);
            float rightLen = Length(c - a);
            float avgLen = (leftLen + rightLen) / 2.f;
            float reqHeight = avgLen * input.firstCellOptValue;

            // !!!!!!!TODO: UPDATE AND CLAMP UI CONTROL!!!!!!!!!!!!
            if (semiMaj + reqHeight < focus)
            {
                ImGui::Text("AR too low!");
            }
            else
            {
                float reqMu = IntersectRadialX(focus, semiMaj + reqHeight);
                if (reqMu > maxmu)
                {
                    ImGui::Text("AR too high!");
                }
                else if (reqMu < surfMu)
                {
                    ImGui::Text("AR too low!");
                }
                else
                {
                    firstMu = reqMu;

                    //a.x = focus * coshf(surfMu) * cos(nus[3]);
                    //a.y = focus * sinhf(surfMu) * sin(nus[3]);
                    //b.x = focus * coshf(surfMu) * cos(nus[2]);
                    //b.y = focus * sinhf(surfMu) * sin(nus[2]);
                    //c.x = focus * coshf(surfMu) * cos(nus[1]);   //nus.back() is i==imax overlapping i==0
                    //c.y = focus * sinhf(surfMu) * sin(nus[1]);

                    //leftLen = Length(b - a);
                    //rightLen = Length(c - a);
                    //avgLen = (leftLen + rightLen) / 2.f;
                    //reqHeight = avgLen * input.firstCellOptValue;

                    //float yy = focus * sinhf(firstMu) * sin(nus[2]);
                    //float dy = yy - semiMin;
                    //__debugbreak();
                }
            }
            break;
        }
        default:
            break;
        }
        
        // Remaining
        int toadd = input.jmax - 2; // Exclude inner and outer boundaries 
        float dmu = (maxmu - firstMu) / toadd;
        for (int i = 0; i < toadd; ++i)
            mus.push_back(firstMu + i * dmu);
    }
    else
    {
        float dmu = (maxmu - surfMu) / (input.jmax - 1);
        for (int i = 0; i < input.jmax; ++i)
            mus.push_back(surfMu + i * dmu);
    }

    std::vector<std::vector<ImVec2>> grid;
    grid.resize(input.imax);
    for (auto& isoline : grid)
        isoline.resize(input.jmax - 1);

    for (int itAngle = 0; itAngle < input.imax; ++itAngle)
    {
        const auto nu = nus[itAngle];

        int jlim = input.jmax - 1;
        /*if (input.firstCellOption == GridParams::HEIGHT)
            jlim = input.jmax - 1;*/

        for (int itRadial = 0; itRadial < jlim; ++itRadial)
        {
            const auto mu = mus[itRadial];
            
            float foc = focus;
            // TODO: make less stupid
            if (itRadial >= 1 && input.firstCellOption == GridParams::HEIGHT && innerFoc > 0.f)
            {
                foc = innerFoc;
            }

            float x = foc * coshf(mu) * cos(nu);
            float y = foc * sinhf(mu) * sin(nu);
            grid[itAngle][itRadial] = ImVec2(x, y);  // (circumf, radial); NOT ~(radial, circumf)~
            drawableGrid.vertsLocal.push_back({ x,y });
        }

        // Corresponding point on outer boundary to angle nu 
        float x = 0.f, y = 0.f;
        
        bool isCorner = false;
        for (auto& corner : cornerBC)
        {
            if (fabs(nu - corner.first) < std::numeric_limits<float>::epsilon())
            {
                x = corner.second.x;
                y = corner.second.y;
                isCorner = true;
                break;
            }
        }
        
        if (!isCorner)
        {
            if (nu < M_PI_4 ||
                nu >(2 * M_PI - M_PI_4))
            {
                // x = Nfar ; y = 0..Nfar
                x = input.nfar;
                float localMu = acoshf(x / (focus * cos(nu)));
                y = focus * sinhf(localMu) * sin(nu);
            }
            else if (nu > M_PI_4&& nu < (M_PI - M_PI_4))
            {
                // x = Nfar..0 ; y = Nfar
                y = input.nfar;
                float localMu = asinhf(y / (focus * sin(nu)));
                x = focus * coshf(localMu) * cos(nu);
            }
            else if (nu > (M_PI - M_PI_4) && nu < (M_PI + M_PI_4))
            {
                // x = -Nfar ; y = 0..Nfar
                x = -input.nfar;
                float localMu = acoshf(x / (focus * cos(nu)));
                y = focus * sinhf(localMu) * sin(nu);
            }
            else if (nu > (M_PI + M_PI_4) && nu < (2 * M_PI - M_PI_4))
            {
                // x = Nfar..0 ; y = Nfar
                y = -input.nfar;
                float localMu = asinhf(y / (focus * sin(nu)));
                x = focus * coshf(localMu) * cos(nu);
            }
        }

        grid[itAngle].push_back(ImVec2(x, y));

        drawableGrid.vertsLocal.push_back({ x,y });
    }

    return grid;
}

std::vector<Drawable> ComputeGridEdges(std::vector<std::vector<ImVec2>> grid)
{
    std::vector<Drawable> gridlines;

    std::vector<ImVec2> isoAng;
    isoAng.resize(grid[0].size());
    for (int i = 0; i < grid.size(); ++i) // Iterate along angular 
    {
        for (int j = 0; j < grid[0].size(); ++j) // Iterate along radial
        {
            isoAng[j] = ImVec2(grid[i][j].x, grid[i][j].y);
        }

        Drawable line;
        line.topology = Drawable::Topology::LINES;
        line.vertsLocal = isoAng;
        gridlines.emplace_back(std::move(line));
    }

    std::vector<ImVec2> isoRad;
    isoRad.resize(grid.size());
    for (int i = 0; i < grid[0].size(); ++i) // Iterate along radial
    {
        for (int j = 0; j < grid.size(); ++j) // Iterate along angular
        {
            isoRad[j] = ImVec2(grid[j][i].x, grid[j][i].y);
        }

        Drawable line;
        line.topology = Drawable::Topology::LINES;
        line.vertsLocal = isoRad;
        gridlines.emplace_back(std::move(line));
    }

    return gridlines;
}

int main(int, char**)
{
    auto window = Init();
    glClearColor(0.f, 0.f, 0.f, 1.f);

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        ImVec2 subWndSz(display_w, display_h);
        ImGui::SetNextWindowSize(subWndSz, ImGuiCond_Always);
        ImGui::SetNextWindowPos(ImVec2(1, 1));

        if (!ImGui::Begin(
            "Sensible inputs",
            nullptr,
            ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar))
        {
            ImGui::End();
            throw "a fit";
        }

        static GridParams input;
        AddUIControls(input);

        // Compute
        Drawable localfoil = ComputeFoil(input.tc);
        Drawable localFoci = ComputeFoci(input.tc);

        Drawable drawableGrid;
        auto grid = ComputeGrid(input, drawableGrid);
        auto gridlines = ComputeGridEdges(grid);

        // Generate canvas
        ImDrawList* drawList = ImGui::GetWindowDrawList();
        auto canvas = CreateAvailableCanvas(*drawList);

        // Draw preview
        std::vector<Drawable*> drawables = { &localfoil, &localFoci, &drawableGrid };
        for (auto& drawable : gridlines) drawables.push_back(&drawable);

        if (input.firstCellOption == GridParams::HEIGHT && innerFoc > 0.f && innerMu > 0.f)
        {
            float x = 0.f, y = 0.f;
            x = innerFoc * coshf(innerMu);
            y = innerFoc * sinh(innerMu);
            Drawable drr;
            drr.vertsLocal.push_back(ImVec2(x, 0.f));
            drr.vertsLocal.push_back(ImVec2(0.f, y));
            drawables.push_back(&drr);
        }

        for (auto& drawable : drawables)
        {
            drawable->UpdateScreenCoords(canvas.pos, canvas.size, input.nfar);
            drawable->Draw(*drawList);
        }

        ImGui::End();

        if (toExport)
        {
            toExport = false;
            std::ofstream file("mesh.plt");
            file << R"(VARIABLES = "X" "Y")";
            file << "\n";
            file << "ZONE I = " << input.imax << ", ";
            file << "J = " << input.jmax << ", ";
            file << "F = POINT" << "\n";

            for (int i = 0; i < grid[0].size(); ++i) // Iterate along radial 
            {
                for (int j = 0; j < grid.size(); ++j) // Iterate along angular
                {
                    file << grid[j][i].x << " " << grid[j][i].y << "\n"; // Blocks: all js for 1st i; 
                                                                         //         all js for 2nd i; etc
                }
            }
        }

        // Rendering
        ImGui::Render();
        glViewport(0, 0, display_w, display_h);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();
}
